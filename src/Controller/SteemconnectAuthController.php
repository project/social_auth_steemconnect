<?php

namespace Drupal\social_auth_steemconnect\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_steemconnect\SteemconnectAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Social Auth Steemconnect routes.
 */
class SteemconnectAuthController extends OAuth2ControllerBase {

  /**
   * SteemconnectAuthController constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_steemconnect network plugin.
   * @param \Drupal\social_auth\User\UserAuthenticator $user_authenticator
   *   Manages user login/registration.
   * @param \Drupal\social_auth_steemconnect\SteemconnectAuthManager $steemconnect_manager
   *   Used to manage authentication methods.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Used to access GET parameters.
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   The Social Auth data handler.
   */
  public function __construct(MessengerInterface $messenger,
                              NetworkManager $network_manager,
                              UserAuthenticator $user_authenticator,
                              SteemconnectAuthManager $steemconnect_manager,
                              RequestStack $request,
                              SocialAuthDataHandler $data_handler) {

    parent::__construct('Social Auth Steemconnect', 'social_auth_steemconnect', $messenger, $network_manager, $user_authenticator, $steemconnect_manager, $request, $data_handler);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('social_auth_steemconnect.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler')
    );
  }

  /**
   * Response for path 'user/login/steemconnect'.
   *
   * Redirects the user to Steemconnect for authentication.
   */
  public function redirectToSteemconnect() {
    /* @var \League\OAuth2\Client\Provider\Steemconnect false $steemconnect*/
    $steemconnect = $this->networkManager->createInstance('social_auth_steemconnect')->getSdk();

    // If Steemconnect client could not be obtained.
    if (!$steemconnect) {
      $this->messenger->addError($this->t('Social Auth Steemconnect not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    // Steemconnect service was returned, inject it to $steemconnectManager.
    $this->steemconnectManager->setClient($steemconnect);

    // Generates the URL where user will be redirected for Steemconnect login.
    // If the user did not have email permission granted on previous attempt,
    // we use the re-request URL requesting only the email address.
    $steemconnect_login_url = $this->steemconnectManager->getAuthorizationUrl();

    $state = $this->steemconnectManager->getState();

    $this->dataHandler->set('oauth2state', $state);

    return new TrustedRedirectResponse($steemconnect_login_url);
  }

  /**
   * Response for path 'user/login/steemconnect/callback'.
   *
   * Steemconnect returns the user here after user has authenticated.
   */
  public function callback() {
    // Checks if user cancel login via Steemconnect.
    $error = $this->request->getCurrentRequest()->get('error');
    if ($error == 'access_denied') {
      $this->messenger->addError($this->t('You could not be authenticated.'));
      return $this->redirect('user.login');
    }

    /* @var \League\OAuth2\Client\Provider\Steemconnect false $steemconnect */
    $steemconnect = $this->networkManager->createInstance('social_auth_steemconnect')->getSdk();

    // If Steemconnect client could not be obtained.
    if (!$steemconnect) {
      $this->messenger->addError($this->t('Social Auth Steemconnect not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    $state = $this->dataHandler->get('oauth2state');

    // Retreives $_GET['state'].
    $retrievedState = $this->request->getCurrentRequest()->query->get('state');
    if (empty($retrievedState) || ($retrievedState !== $state)) {
      $this->userManager->nullifySessionKeys();
      $this->messenger->addError($this->t('Steemconnect login failed. Unvalid oAuth2 State.'));
      return $this->redirect('user.login');
    }

    // Saves access token to session.
    $this->dataHandler->set('access_token', $this->steemconnectManager->getAccessToken());

    $this->steemconnectManager->setClient($steemconnect)->authenticate();

    // Gets user's info from Steemconnect API.
    /* @var \League\OAuth2\Client\Provider\SteemconnectResourceOwner $profile */
    if (!$profile = $this->steemconnectManager->getUserInfo()) {
      $this->messenger->addError($this->t('Steemconnect login failed, could not load Steemconnect profile. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    // Gets (or not) extra initial data.
    $data = $this->userManager->checkIfUserExists($profile->getId()) ? NULL : $this->steemconnectManager->getExtraDetails();

    // If user information could be retrieved.
    // Note: getEmail() in guix77/oauth2-steemconnect sends a fake email like:
    // 123456@@fake-steemconnect-email.com (account ID@...)
    // Indeed, Steemconnect does not provide emails.
    return $this->userManager->authenticateUser($profile->getName(), $profile->getEmail(), $profile->getId(), $this->steemconnectManager->getAccessToken(), $profile->getProfileImage(), $data);
  }

}
