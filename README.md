CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How it works
 * Support requests
 * Maintainers
 * Credits
 * Donations


 INTRODUCTION
 ------------

 Social Auth SteemConnect Module is a SteemConnect authentication integration for
 Drupal. It is based on the Social Auth and Social API projects.

 It adds to the site:
  * A new url: /user/login/steemconnect.
  * A settings form at /admin/config/social-api/social-auth/steemconnect.
  * A SteemConnect logo in the Social Auth Login block.

 REQUIREMENTS
 ------------

This module requires the following modules:

 * Social Auth (https://drupal.org/project/social_auth), branch 2.x
 * Social API (https://drupal.org/project/social_api), branch 2.x

 INSTALLATION
 ------------

 * Run composer to install dependencies:
   composer require "drupal/social_auth_steemconnect:~2.0"

 * Install the dependencies: Social API and Social Auth:
   composer require drupal/social_api:2.x
   composer require drupal/social_auth:2.x

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

 CONFIGURATION
 -------------

 * Add your SteemConnect project OAuth information in
   Configuration » User Authentication » SteemConnect.

 * Place a Social Auth SteemConnect block in Structure » Block Layout.

 * If you already have a Social Auth Login block in the site, rebuild the cache.

 HOW IT WORKS
 ------------

 User can click on the SteemConnect logo on the Social Auth Login block
 You can also add a button or link anywhere on the site that points
 to /user/login/steemconnect, so theming and customizing the button or link
 is very flexible.

 When the user opens the /user/login/steemconnect link, it automatically takes
 user to SteemConnect Accounts for authentication. SteemConnect then returns the user to
 Drupal site. If we have an existing Drupal user with the same email address
 provided by SteemConnect, that user is logged in. Otherwise a new Drupal user is
 created.

 SUPPORT REQUESTS
 ----------------

* Before posting a support request, carefully read the installation
  instructions provided in module documentation page.

* Before posting a support request, check Recent log entries at
  admin/reports/dblog

* Once you have done this, you can post a support request at module issue
  queue: https://www.drupal.org/project/issues/social_auth_steemconnect

* When posting a support request, please inform if you were able to see any
  errors in the Recent Log entries.

 MAINTAINERS
 -----------

 Current maintainer:
  * Guillaume Duveau (https://www.drupal.org/user/173213)

 CREDITS
 -------

 Original developer:
  * Guillaume Duveau (https://www.drupal.org/user/173213)

 Original idea and proposal by:
  * Gokul NK (https://www.drupal.org/u/gokulnk)

Gokul NK posted a proposal of this module on https://steemit.com/utopian-io/@gokulnk/socialauth-steemconnect-module-for-drupal, registred the Drupal project on https://www.drupal.org/project/social_auth_steemconnect. Guillaume Duveau developed this Drupal module and the Steemconnect oAuth2 provider for the League oAuth2 client.

This module was created using https://www.drupal.org/project/social_auth_github as a template.

 DONATIONS
 ---------

If you want to support the original module developer and current maintainer, donations are welcome at:
* Steemit: @guix77 / https://steemit.com/@guix77
* Bitcoin: 12BkpRaeAPcUvxft5yWrSxtknVe7jNUNer
* Ethereum and Ethereum-based (ERC-20) Tokens: 0xa32e0e0a0C8Aa10ac70a2827cB20641850824eEc
* Cardano: DdzFFzCqrhsfUhaNNWuV1BSgjXC5ySNMhfPFRASXTkj26qCKoyYSbW3qWzQucuVLU2AE6AzuYiEfgHnc3DwhSyofeqZZxzANVMLBpDxp
* Paypal : guillaume.duveau@gmail.com
